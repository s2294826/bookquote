package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {


    public double getBookPrice(String isbn) {
        HashMap<String, Double> num = new HashMap<String, Double>();
        num.put("1", 10.0);
        num.put("2", 45.0);
        num.put("3", 20.0);
        num.put("4", 35.0);
        num.put("5", 50.0);
        num.put("others", 0.0);


        return num.get(isbn);
    }
}
